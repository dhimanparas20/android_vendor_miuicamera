PRODUCT_SOONG_NAMESPACES += \
    vendor/miuicamera

PRODUCT_COPY_FILES += \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/etc,$(TARGET_COPY_OUT_SYSTEM)/etc) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/lib64,$(TARGET_COPY_OUT_SYSTEM)/lib64) \
    $(call find-copy-subdir-files,*,vendor/miuicamera/proprietary/priv-app/MiuiCamera/lib,$(TARGET_COPY_OUT_SYSTEM)/priv-app/MiuiCamera/lib)

PRODUCT_PACKAGES += \
    MiuiCamera

PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.google.lens.oem_camera_package=com.android.camera

include vendor/miuicamera/BoardConfigAnx.mk
